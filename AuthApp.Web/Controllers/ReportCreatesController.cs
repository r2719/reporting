﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using AuthApp.Web.Database;
using AuthApp.Web.Entities;
using CrystalDecisions.CrystalReports.Engine;

namespace AuthApp.Web.Controllers
{
   
    public class ReportCreatesController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: ReportCreates
        public ActionResult Index()
        {
            var loggedInUser = Session["username"];
            var userFullName = Session["fullName"];

            ViewBag.Username = loggedInUser;
            ViewBag.FullName = userFullName;
            return View(db.ReportCreates.ToList());
        }

        // GET: ReportCreates/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ReportCreate reportCreate = db.ReportCreates.Find(id);
            if (reportCreate == null)
            {
                return HttpNotFound();
            }
            return View(reportCreate);
        }

        // GET: ReportCreates/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: ReportCreates/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Username,Nationality,ReportNo,CnicNo,DOB,SampleNo,TestDate,ResultDate,Result,ResultArabic")] ReportCreate reportCreate)
        {
            if (ModelState.IsValid)
            {
                db.ReportCreates.Add(reportCreate);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(reportCreate);
        }

        // GET: ReportCreates/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ReportCreate reportCreate = db.ReportCreates.Find(id);
            if (reportCreate == null)
            {
                return HttpNotFound();
            }
            return View(reportCreate);
        }

        // POST: ReportCreates/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Username,Nationality,ReportNo,CnicNo,DOB,SampleNo,TestDate,ResultDate,Result,ResultArabic")] ReportCreate reportCreate)
        {
            if (ModelState.IsValid)
            {
                db.Entry(reportCreate).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(reportCreate);
        }

        // GET: ReportCreates/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ReportCreate reportCreate = db.ReportCreates.Find(id);
            if (reportCreate == null)
            {
                return HttpNotFound();
            }
            return View(reportCreate);
        }

        // POST: ReportCreates/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            ReportCreate reportCreate = db.ReportCreates.Find(id);
            db.ReportCreates.Remove(reportCreate);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
        public ActionResult ExportPatient(int id)
        {
            ReportCreate ReportCreate = db.ReportCreates.FirstOrDefault(x => x.Id == id);

            //var vm = new ReportCreate
            //{
            //    Username = ReportCreates.Username,
            //    ReportNo = ReportCreates.ReportNo,
            //    SampleNo = ReportCreates.SampleNo,
            //    CnicNo = ReportCreates.CnicNo,
            //    TestDate = ReportCreates.TestDate,
            //    ResultDate = ReportCreates.ResultDate,
            //    Nationality = ReportCreates.Nationality,
            //    DOB = ReportCreates.DOB,
            //    Result = ReportCreates.Result,
            //    ResultArabic = ReportCreates.ResultArabic
            //};

            ReportDocument rd = new ReportDocument();
            var path = Path.Combine(Server.MapPath("~/Report"), "Reports.rpt");
            rd.Load(Path.Combine(Server.MapPath("~/Report"), "Reports.rpt"));

            rd.SetDataSource(db.ReportCreates.Where(x => x.Id == id).ToList());

            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();


            Stream stream = rd.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
            stream.Seek(0, SeekOrigin.Begin);
            return File(stream, "application/pdf", "PatientReport.pdf");
        }
    }
}
