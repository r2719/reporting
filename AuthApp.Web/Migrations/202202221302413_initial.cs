﻿namespace AuthApp.Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ReportCreateModels",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Username = c.String(),
                        Nationality = c.String(),
                        ReportNo = c.String(),
                        CnicNo = c.String(),
                        DOB = c.DateTime(nullable: false),
                        SampleNo = c.String(),
                        TestDate = c.DateTime(nullable: false),
                        ResultDate = c.DateTime(nullable: false),
                        Result = c.String(),
                        ResultArabic = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.ReportCreates",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Username = c.String(),
                        Nationality = c.String(),
                        ReportNo = c.String(),
                        CnicNo = c.String(),
                        DOB = c.DateTime(nullable: false),
                        SampleNo = c.String(),
                        TestDate = c.DateTime(nullable: false),
                        ResultDate = c.DateTime(nullable: false),
                        Result = c.String(),
                        ResultArabic = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Users",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Username = c.String(),
                        Password = c.String(),
                        FirstName = c.String(),
                        LastName = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Users");
            DropTable("dbo.ReportCreates");
            DropTable("dbo.ReportCreateModels");
        }
    }
}
