﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AuthApp.Web.Models
{
    public class ReportCreateModel
    {
        public int Id { get; set; }
        public string Username { get; set; }
        public string Nationality { get; set; }
        public string ReportNo { get; set; }
        public string CnicNo { get; set; }
        public DateTime DOB { get; set; }
        public string SampleNo { get; set; }
        public DateTime TestDate { get; set; }
        public DateTime ResultDate { get; set; }
        public string Result { get; set; }
        public string ResultArabic { get; set; }
    }
}