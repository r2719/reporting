﻿using AuthApp.Web.Report;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using CrystalDecisions.Web;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AuthApp.Web.Report_Forms
{
    public partial class Report : System.Web.UI.Page
    {


        protected void Page_Load(object sender, EventArgs e)
        {
            
            Dictionary<string, string> param = CommonFunctions.ParameterFilter(Request,
              new List<string>() { "Para_Id" });

            Reports report = new Reports();

            //CommonFunctions.LoadReport(
            //    report,
            //    CrystalReportViewer1,
            //    new List<KeyValuePair<IParameterField, string>>()
            //    {
            //        new KeyValuePair<IParameterField, string>(report,  param["Para_Id"])

            //    }
            //    );
        }


        //ReportDocument rprt = new ReportDocument();
        //protected void Page_Load(object sender, EventArgs e)
        //{

        //}
        //protected void Button1_Click(object sender, EventArgs e)
        //{
        //    rprt.Load(Server.MapPath("~/Report/Report.rpt"));
        //    SqlConnection con = new SqlConnection(@"Data Source=192.168.9.7\SQLEXPRESS;AttachDbFilename=|DataDirectory|\Database.mdf;Integrated Security=True;User Instance=True");
        //    SqlCommand cmd = new SqlCommand("sp_select", con);
        //    SqlDataAdapter sda = new SqlDataAdapter(cmd);
        //    DataTable dt = new DataTable();
        //    sda.Fill(dt);
        //    rprt.SetDataSource(dt);
        //    rprt.SetParameterValue("age1", CrystalReportViewer1.Text);
        //    CrystalReportViewer1.ReportSource = rprt;
        //    CrystalReportViewer1.DataBind();
        //}
    }
}
public static class CommonFunctions
{

    #region Reporting Function 

    /// <summary>
    ///  This function is used for setting report connection
    ///  Note: you need explicit casting to avoid error 
    ///  Developed By : Farhan Shahid
    /// </summary>
    /// <param name="report">Report Object</param>
    /// <returns>Return Report Class Object</returns>
    public static ReportClass SetReportConnection(ReportClass report)
    {
        try
        {
            string connectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            var conBuilder = new SqlConnectionStringBuilder(connectionString);
            var strServer = conBuilder.DataSource.ToString();
            var strDatabase = conBuilder.InitialCatalog;
            var strUserID = conBuilder.UserID;
            var strPwd = conBuilder.Password;
            report.DataSourceConnections[0].IntegratedSecurity = false;
            report.DataSourceConnections[0].SetConnection(strServer, strDatabase, strUserID, strPwd);
            report.SetDatabaseLogon(strUserID, strPwd);
        }
        catch (Exception ex)
        {
            throw;
        }
        return report;
    }

    /// <summary>
    /// Set Report Connection & Parameters
    /// Developed By : Farhan Shahid
    /// </summary>
    /// <param name="report">Report Object</param>
    /// <param name="param">List of parameters</param>
    /// <returns>Report Object</returns>
    public static ReportClass SetReportParameters(ReportClass report, List<KeyValuePair<IParameterField, string>> param)
    {
        report = SetReportConnection(report);

        try
        {
            foreach (var item in param)
            {

                if (item.Key.ParameterValueKind.ToString() == "NumberParameter")
                {
                    report.SetParameterValue(item.Key.ParameterFieldName, string.IsNullOrEmpty(item.Value) ? new ParameterDiscreteValue().Value = null : Convert.ToInt32(item.Value));
                }
                else if (item.Key.ParameterValueKind.ToString() == "DateParameter")
                {
                    report.SetParameterValue(item.Key.ParameterFieldName, string.IsNullOrEmpty(item.Value) ? new ParameterDiscreteValue().Value = null : Convert.ToDateTime(item.Value));
                }
                else if (item.Key.ParameterValueKind.ToString() == "DateTimeParameter")
                {
                    report.SetParameterValue(item.Key.ParameterFieldName, string.IsNullOrEmpty(item.Value) ? new ParameterDiscreteValue().Value = null : Convert.ToDateTime(item.Value));
                }
                else if (item.Key.ParameterValueKind.ToString() == "DateParameter")
                {
                    report.SetParameterValue(item.Key.ParameterFieldName, string.IsNullOrEmpty(item.Value) ? new ParameterDiscreteValue().Value = null : Convert.ToDateTime(item.Value).Date);
                }
                else if (item.Key.ParameterValueKind.ToString() == "BooleanParameter")
                {
                    report.SetParameterValue(item.Key.ParameterFieldName, string.IsNullOrEmpty(item.Value) ? new ParameterDiscreteValue().Value = null : Convert.ToBoolean(item.Value));
                }
                else
                {
                    report.SetParameterValue(item.Key.ParameterFieldName, string.IsNullOrEmpty(item.Value) ? new ParameterDiscreteValue().Value = null : item.Value);
                }
            }
            return report;
        }
        catch (Exception)
        {
            throw;
        }

    }


    /// <summary>
    /// Load report without parameters
    /// Developed By : Farhan Shahid
    /// </summary>
    /// <param name="report">Report Object</param>
    /// <param name="view">Crystal Report View Object</param>
    public static void LoadReport(ReportClass report, CrystalReportViewer view)
    {
        report = SetReportConnection(report);
        try
        {
            view.ReportSource = report;
        }
        catch (Exception)
        {
            throw;
        }
    }

    /// <summary>
    /// Load report with dataset
    /// Developed By : Farhan Shahid
    /// </summary>
    /// <param name="report">Report Object</param>
    /// <param name="view">Crystal Report View Object</param>
    /// <param name="ds">Dataset</param>

    public static void LoadReport(ReportClass report, CrystalReportViewer view, System.Data.DataSet ds)
    {
        report = SetReportConnection(report);

        try
        {
            report.SetDataSource(ds);
            view.ReportSource = report;
        }
        catch (Exception)
        {
            throw;
        }
    }

    /// <summary>
    /// Load report with parameters
    /// Developed By : Farhan Shahid
    /// </summary>
    /// <param name="report">Report Object</param>
    /// <param name="view">Crystal Report View Object</param>
    /// <param name="param">Pass List of paramters inform of keyValuePair</param>

    public static void LoadReport(ReportClass report, CrystalReportViewer view, List<KeyValuePair<IParameterField, string>> param)
    {
        try
        {
            report = SetReportParameters(report, param);
            view.ReportSource = report;
        }
        catch (Exception ex)
        {
            throw;
        }
    }

    /// <summary>
    /// Load report with parameters & view type
    /// Developed By : Farhan Shahid
    /// </summary>
    /// <param name="report">Report Object</param>
    /// <param name="view">Crystal Report View Object</param>
    /// <param name="viewType"> Either you show Group Tree/Paramter Panle or None</param>
    /// <param name="param">Pass List of paramters inform of keyValuePair</param>

    public static void LoadReport(ReportClass report, CrystalReportViewer view, ToolPanelViewType viewType, List<KeyValuePair<IParameterField, string>> param)
    {
        report = SetReportParameters(report, param);
        view.ToolPanelView = viewType;
        view.ReportSource = report;
    }


    #endregion

    #region Parameter Filteration
    /// <summary>
    /// Get Http Request Object & Check each Query String value for NULL or Empty 
    /// Developed By : Farhan Shahid
    /// </summary>
    /// <param name="req">HttpRequest Object</param>
    /// <param name="param">List of Query string</param>
    /// <returns></returns>
    public static Dictionary<string, string> ParameterFilter(HttpRequest req, List<string> param)
    {
        Dictionary<string, string> obj = new Dictionary<string, string>();
        foreach (var item in param)
        {
            obj.Add(
                item,
                string.IsNullOrEmpty(req.QueryString[item]) ?
                string.Empty :
                ((req.QueryString[item].ToString() == "? undefined:undefined ?" || req.QueryString[item].ToString() == "undefined") ?
                    string.Empty : req.QueryString[item].ToString())
                 );
        }

        return obj;
    }
    #endregion


    public static DataTable ToDataTable<T>(List<T> items)
    {
        DataTable dataTable = new DataTable(typeof(T).Name);
        //Get all the properties
        PropertyInfo[] Props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
        foreach (PropertyInfo prop in Props)
        {
            //Setting column names as Property names
            dataTable.Columns.Add(prop.Name);
        }
        foreach (T item in items)
        {
            var values = new object[Props.Length];
            for (int i = 0; i < Props.Length; i++)
            {
                //inserting property values to datatable rows
                values[i] = Props[i].GetValue(item, null);
            }
            dataTable.Rows.Add(values);
        }
        //put a breakpoint here and check datatable
        return dataTable;
    }
}
